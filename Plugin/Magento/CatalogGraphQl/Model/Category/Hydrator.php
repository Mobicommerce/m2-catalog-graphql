<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_CatalogGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\CatalogGraphQl\Plugin\Magento\CatalogGraphQl\Model\Category;

class Hydrator
{
    public function __construct(
        \Magento\Catalog\Helper\Category $categoryHelper
    ) {
        $this->categoryHelper = $categoryHelper;
    }

    public function afterHydrateCategory($subject, $result, $category)
    {
        $result['url_key'] = $this->categoryHelper->getCategoryUrl($category);
        return $result;
    }
}
